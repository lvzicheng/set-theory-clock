package com.paic.arch.interviews;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author lvzicheng
 *
 */
public class TimeConverterImpl implements TimeConverter {

	private static final Logger logger = LoggerFactory.getLogger(TimeConverterImpl.class);

	@Override
	public String convertTime(String convertTime) {
		
		try{
			
			DateFormat date = new SimpleDateFormat("HH:mm:ss");
			
			date.parse(convertTime);
			
		}catch(ParseException e){
			
			logger.error("转换时间{}格式错误！", convertTime);
			
			throw new RuntimeException("转换时间" + convertTime + "格式错误！");
		}
		
		String[] timeStrs = convertTime.split(":");
		
		int hours = Integer.parseInt(timeStrs[0]);
		
		int minutes = Integer.parseInt(timeStrs[1]);
		
		int seconds = Integer.parseInt(timeStrs[2]);
		
		String[] results = new String[5];
		
		//处理顶端的秒
		convertSecond(seconds, results);
		
		//处理第一排小时
		convertFirstLineHour(hours, results);
		
		//处理第二排小时
		convertSecondLineHour(hours, results);
		
		//处理第三排分钟
		convertThirdLineMinute(minutes, results);
		
		//处理第四排分钟
		convertFourthLineMinute(minutes, results);
		
		 return String.join(System.getProperty("line.separator"),results);
				
	}
    
	/**
	 * 顶端秒转换
	 * @param seconds
	 * @param results
	 */
	private  void convertSecond(Integer seconds, String[] results){
		
		results[0] = seconds % 2 == 0 ? "Y" : "O";
	}

	/**
	 * 处理第一排小时，每盏灯代表5小时，使用除法获取需要亮灯的个数
	 * @param hours
	 * @param results
	 */
	private  void convertFirstLineHour(Integer hours, String[] results){
		
		int fiveHourNums = hours / 5;
		
		results[1] = replaceStr( 0, fiveHourNums, "OOOO", "R");
		
	}
	
	/**
	 * 处理第二排小时，将传入小时数对5取余，获取代表1小时的亮灯个数
	 * @param hours
	 * @param results
	 */
	private void convertSecondLineHour(Integer hours, String[] results){
		
		int hourNums = hours % 5;
		
		results[2] = replaceStr( 0, hourNums, "OOOO", "R");
	}
	
	/**
	 * 处理第三排分钟，每盏灯代表5分钟，使用除法获取表示5分钟亮灯的个数
	 * @param minutes
	 * @param results
	 */
	private void convertThirdLineMinute(Integer minutes, String[] results){
		
		int minuteNums = minutes / 5;
		
		results[3] = replaceStr(minuteNums, 11, "YYRYYRYYRYY", "O");
	}
	
	/**
	 * 处理第四排分钟，将传入分钟数对5取余，获取需要亮灯的个数
	 * @param minutes
	 * @param results
	 */
	private void convertFourthLineMinute(Integer minutes, String[] results){
		
		int minuteNums = minutes % 5;
		
		results[4] = replaceStr(0, minuteNums, "OOOO", "Y");
	}
	
	/**
	 * 根据计算结果替换默认字符串
	 * @param start
	 * @param end
	 * @param defaultStr
	 * @param replaceStr
	 * @return
	 */
	private String replaceStr( int start, int end, String defaultStr, String replaceStr) {
		
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < end - start; i++) {
			
			sb.append(replaceStr);
			
		}
		
		return new StringBuffer(defaultStr).replace(start, end, sb.toString()).toString();
	}

}
